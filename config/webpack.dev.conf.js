const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const CleanWebpackPlugin = require('clean-webpack-plugin')

const base = require('./webpack.base.conf')

const confDev = {
  mode: 'development',
  devtool: 'eval-source-map',
  output: {
    filename: 'js/[name].app.js',
    publicPath: '/'
  },
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },
  module: {
    rules: [{
      test: /(\.scss|\.css)$/,
      use: [
        {
          loader: 'style-loader'
        },
        {
          loader: 'css-loader',
          options: {
            sourceMap: true
          }
        },
        {
          loader: 'resolve-url-loader'
        },
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true
          }
        }
      ]
    }]
  },
  plugins: [
    new CleanWebpackPlugin(['dist'], {
      root: path.join(__dirname, '..'),
      verbose: false
    }),

    new webpack.HotModuleReplacementPlugin(),

    new MiniCssExtractPlugin({
      filename: "css/[name].css",
      chunkFilename: "css/[id].css"
    }),

    new HtmlWebpackPlugin({
      template: './src/pug/index.pug',
      favicon: 'favicon.ico'
    }),

    new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(false)
    })
  ]
}

module.exports = merge(base, confDev)
