module.exports = {
  extends: [
    'airbnb-base'
  ],
  parser: 'babel-eslint',
  env: {
    browser: true,
    es6: true,
    node: true
  },
  globals: {
    PRODUCTION: true
  },
  rules: {
    "semi": [2, "never"],
    "comma-dangle": ["error", "never"],
    "arrow-parens": ["error", "as-needed"],
    "no-console": 0
  }
}
